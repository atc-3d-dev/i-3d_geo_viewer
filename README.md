# i-3D_GEO_viewer

interactive 3D template with GEOINT i-3D_Template a general purpose viewer for GEOINT data including terrain, dted, dem, lidar, or other elevation information. My preference is to import elevation data and turn into game engine terrain